export class Store {
  // All the subscribers are held in this array of functions.
  private subscribers: Function[];
  // The reducers are stored in this object as functions.
  private reducers: { [key: string]: Function };
  // The state object expects all sorts of values.
  private state: { [key: string]: any };

  /**
   * CONSTRUCTOR
   * Params:
   * reducers = Object = An object holding the reducer function(S)
   * initialState = Object = An object that stores the application state.
  */
  constructor(reducers = {}, initialState = {}) {
    this.subscribers = [];
    this.reducers = reducers;
    this.state = this.reduce(initialState, {});
  }

  get value(): object {
    return this.state;
  }

  subscribe(fn) {
    this.subscribers = [...this.subscribers, fn];
    this.notify();
    return () => {
      this.subscribers = this.subscribers.filter(sub => sub !== fn);
    }
  }

  private notify() {
    this.subscribers.forEach(fn => fn(this.value));
  }

  dispatch(action) {
    this.state = this.reduce(this.state, action);
    this.notify();
  }

  private reduce(state, action) {
    const newState = {};
    for (const prop in this.reducers) {
      newState[prop] = this.reducers[prop](state[prop], action);
    }
    return newState;
  }
}