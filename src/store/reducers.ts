import * as fromActions from '../store';

export const initialState = {
  loaded: false,
  loading: false,
  data: [{ label: 'Eat pizza', complete: false }]
};

export function reducer(
  state = initialState,
  action: { type: string, payload: any }
) {
  switch(action.type) {
    case fromActions.ADD_TODO: {
      const todo = action.payload;
      // Copying the data array's content from our current state and merging it
      // with the todo object coming from the action's payload
      const data = [...state.data, todo];
      // The reducer is returning an updated copy of the previous state
      // with the merged changes
      return {
        ...state,
        data,
      };
    }

    case fromActions.REMOVE_TODO: {
      const data = state.data.filter(
        todo => todo.label !== action.payload.label
      );
      return {
        ...state,
        data
      };
    }
  }
  return state;
}