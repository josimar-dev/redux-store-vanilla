import * as fromStore from './store';
import { renderTodos } from './utils';

const input = document.querySelector('input') as HTMLInputElement;
const button = document.querySelector('button') as HTMLButtonElement;
const destroy = document.querySelector('.unsubscribe') as HTMLButtonElement;
const todoList = document.querySelector('.todos') as HTMLLIElement;

// This is an object that holds our reducer functions
// also, we have the current state defined here as well
const reducers = {
  todos: fromStore.reducer
};

// Here we pass the reducers object into the instantiation of 
// the store, since it expects a reducer object as a first parameter in the constructor
const store = new fromStore.Store(reducers);

console.log(store.value);
console.log('===========================');

button.addEventListener(
  'click',
  () => {
    if (!input.value.trim()) return;

    const todo = { label: input.value, complete: false };

    // Dispatching an action called ADD_TODO
    store.dispatch(new fromStore.AddTodo(todo));

    console.log(store.value);

    input.value = '';
  },
  false
);

const unsubscribe = store.subscribe(state => {
  renderTodos(state.todos.data);
});

destroy.addEventListener('click', unsubscribe, false);

todoList.addEventListener('click', function(event) {
  const target = event.target as HTMLButtonElement;
  if (target.nodeName.toLowerCase() === 'button') {
    const todo = JSON.parse(target.getAttribute('data-todo') as any);
    store.dispatch(new fromStore.RemoveTodo(todo));
  }
});

store.subscribe(state => console.log('STATE:::', state));
